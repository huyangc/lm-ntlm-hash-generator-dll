// LM_NTLM Hash Generator.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "LM_NTLM Hash Generator.h"
#include<string>
#include<iostream>
#include<stdlib.h>


using namespace std;


LM_NTLM_API unsigned char* __stdcall LM_Hash_Generator(wchar_t* content){
	//以下代码将wchar_t*转化为char* content->char_string
	size_t wchar_size = wcslen(content)+1;
	size_t converted = 0;
	const size_t newsize = wchar_size*2;
	char *char_string = new char[newsize];
	wcstombs_s(&converted,char_string,wchar_size,content,_TRUNCATE);
	
	//LM Hash Generate
	unsigned char * dst=(unsigned char *)malloc(16);
	int dst_len;
	
	
	char buff1[100];
	strcpy(buff1,char_string);
	unsigned char buff2[512];
	int len=0;
	for(int t=0;t<strlen(buff1);t++)
	{
		buff2[len++]=buff1[t];
		buff2[len++]=0x00;
	}
	
	lm_hash(buff1,dst,&dst_len);
	
	

	return dst;

	
}
LM_NTLM_API unsigned char* __stdcall NTLM_Hash_Generator(wchar_t* content){
	//以下代码将wchar_t*转化为char* content->char_string
	size_t wchar_size = wcslen(content)+1;
	size_t converted = 0;
	const size_t newsize = wchar_size*2;
	char *char_string = new char[newsize];
	wcstombs_s(&converted,char_string,wchar_size,content,_TRUNCATE);

	char buff1[100];
	strcpy(buff1,char_string);
	unsigned char buff2[512];
	int len=0;
	for(int t=0;t<strlen(buff1);t++)
	{
		buff2[len++]=buff1[t];
		buff2[len++]=0x00;
	}
	//NTLM hash
	unsigned char pTemp[256];
	MD4_CTX m_md4;
	MD4Init(&m_md4); //11
	MD4Update(&m_md4, buff2, len);  //222
	MD4Final(pTemp, &m_md4);
	
	
	return pTemp;
}

