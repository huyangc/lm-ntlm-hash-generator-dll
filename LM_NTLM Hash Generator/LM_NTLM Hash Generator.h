#define LM_NTLM_API __declspec(dllexport)
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include<iostream>
#include<memory>
using namespace std;
extern "C" LM_NTLM_API unsigned char* __stdcall LM_Hash_Generator(wchar_t* content);
extern "C" LM_NTLM_API unsigned char* __stdcall NTLM_Hash_Generator(wchar_t* content);


//MD4的有关部分
//以下部分同样来自于网上资源，同openssl api中的MD4.h文件中定义的函数作用一致

#ifndef MD4_POINTER
typedef unsigned char * MD4_POINTER;
#endif

#ifndef UINT4
typedef unsigned long UINT4;
#endif

// MD4 context
typedef struct {
	UINT4 state[4];                                   // state (ABCD)
	UINT4 count[2];        // number of bits, modulo 2^64 (lsb first)
	unsigned char buffer[64];                         // input buffer
} MD4_CTX;

static void MD4Init(MD4_CTX *context);
static void MD4Update(MD4_CTX *context, unsigned char *input, unsigned int inputLen);
static void MD4Final(unsigned char *digest, MD4_CTX *context);

// Constants for MD4_Transform routine.
#define MD4_S11 3
#define MD4_S12 7
#define MD4_S13 11
#define MD4_S14 19
#define MD4_S21 3
#define MD4_S22 5
#define MD4_S23 9
#define MD4_S24 13
#define MD4_S31 3
#define MD4_S32 9
#define MD4_S33 11
#define MD4_S34 15

static void MD4_Transform(UINT4 *state, unsigned char *block);
static void MD4_Encode(unsigned char *output, UINT4 *input, unsigned int len);
static void MD4_Decode(UINT4 *output, unsigned char *input, unsigned int len);

static unsigned char MD4_PADDING[64] = {
	0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

// MD4F, MD4G and MD4H are basic MD4 functions.
#define MD4F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define MD4G(x, y, z) (((x) & (y)) | ((x) & (z)) | ((y) & (z)))
#define MD4H(x, y, z) ((x) ^ (y) ^ (z))

// MD4_ROTL rotates x left n bits.
#define MD4_ROTL(x, n) (((x) << (n)) | ((x) >> (32-(n))))

// MD4FF, MD4GG and MD4HH are transformations for rounds 1, 2 and 3
// Rotation is separate from addition to prevent recomputation
#define MD4FF(a, b, c, d, x, s) { \
	(a) += MD4F ((b), (c), (d)) + (x); \
	(a) = MD4_ROTL ((a), (s)); \
	}
#define MD4GG(a, b, c, d, x, s) { \
	(a) += MD4G ((b), (c), (d)) + (x) + (UINT4)0x5a827999; \
	(a) = MD4_ROTL ((a), (s)); \
	}
#define MD4HH(a, b, c, d, x, s) { \
	(a) += MD4H ((b), (c), (d)) + (x) + (UINT4)0x6ed9eba1; \
	(a) = MD4_ROTL ((a), (s)); \
	}

// MD4 initialization. Begins an MD4 operation, writing a new context
void MD4Init(MD4_CTX *context)
{
	context->count[0] = context->count[1] = 0;

	// Load magic initialization constants
	context->state[0] = 0x67452301;
	context->state[1] = 0xefcdab89;
	context->state[2] = 0x98badcfe;
	context->state[3] = 0x10325476;
}

// MD4 block update operation. Continues an MD4 message-digest
//     operation, processing another message block, and updating the
//     context
void MD4Update(MD4_CTX *context, unsigned char *input, unsigned int inputLen)
{
	unsigned int i = 0, index = 0, partLen = 0;

	// Compute number of bytes mod 64
	index = (unsigned int)((context->count[0] >> 3) & 0x3F);

	// Update number of bits
	if ((context->count[0] += ((UINT4)inputLen << 3))
		< ((UINT4)inputLen << 3))
		context->count[1]++;
	context->count[1] += ((UINT4)inputLen >> 29);

	partLen = 64 - index;

	// Transform as many times as possible
	if (inputLen >= partLen)
	{
		memcpy((MD4_POINTER)&context->buffer[index], (MD4_POINTER)input, partLen);
		MD4_Transform(context->state, context->buffer);
  
		for (i = partLen; i + 63 < inputLen; i += 64)
			MD4_Transform (context->state, &input[i]);

		index = 0;
	}
	else i = 0;
  
	// Buffer remaining input
	memcpy((MD4_POINTER)&context->buffer[index], (MD4_POINTER)&input[i], inputLen - i);
}

// MD4 finalization. Ends an MD4 message-digest operation, writing the
//     the message digest and zeroizing the context.
void MD4Final(unsigned char *digest, MD4_CTX *context)
{
	unsigned char bits[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	unsigned int index = 0, padLen = 0;

	// Save number of bits
	MD4_Encode (bits, context->count, 8);

	// Pad out to 56 mod 64.
	index = (unsigned int)((context->count[0] >> 3) & 0x3f);
	padLen = (index < 56) ? (56 - index) : (120 - index);
	MD4Update(context, MD4_PADDING, padLen);

	// Append length (before padding)
	MD4Update(context, bits, 8);

	// Store state in digest
	MD4_Encode(digest, context->state, 16);

	// Zeroize sensitive information
	memset((MD4_POINTER)context, 0, sizeof(MD4_CTX));
}

// MD4 basic transformation. Transforms state based on block.
static void MD4_Transform(UINT4 *state, unsigned char *block)
{
	UINT4 a = state[0], b = state[1], c = state[2], d = state[3], x[16];

	MD4_Decode(x, block, 64);

	// Round 1
	MD4FF (a, b, c, d, x[ 0], MD4_S11); /* 1 */
	MD4FF (d, a, b, c, x[ 1], MD4_S12); /* 2 */
	MD4FF (c, d, a, b, x[ 2], MD4_S13); /* 3 */
	MD4FF (b, c, d, a, x[ 3], MD4_S14); /* 4 */
	MD4FF (a, b, c, d, x[ 4], MD4_S11); /* 5 */
	MD4FF (d, a, b, c, x[ 5], MD4_S12); /* 6 */
	MD4FF (c, d, a, b, x[ 6], MD4_S13); /* 7 */
	MD4FF (b, c, d, a, x[ 7], MD4_S14); /* 8 */
	MD4FF (a, b, c, d, x[ 8], MD4_S11); /* 9 */
	MD4FF (d, a, b, c, x[ 9], MD4_S12); /* 10 */
	MD4FF (c, d, a, b, x[10], MD4_S13); /* 11 */
	MD4FF (b, c, d, a, x[11], MD4_S14); /* 12 */
	MD4FF (a, b, c, d, x[12], MD4_S11); /* 13 */
	MD4FF (d, a, b, c, x[13], MD4_S12); /* 14 */
	MD4FF (c, d, a, b, x[14], MD4_S13); /* 15 */
	MD4FF (b, c, d, a, x[15], MD4_S14); /* 16 */

	// Round 2
	MD4GG (a, b, c, d, x[ 0], MD4_S21); /* 17 */
	MD4GG (d, a, b, c, x[ 4], MD4_S22); /* 18 */
	MD4GG (c, d, a, b, x[ 8], MD4_S23); /* 19 */
	MD4GG (b, c, d, a, x[12], MD4_S24); /* 20 */
	MD4GG (a, b, c, d, x[ 1], MD4_S21); /* 21 */
	MD4GG (d, a, b, c, x[ 5], MD4_S22); /* 22 */
	MD4GG (c, d, a, b, x[ 9], MD4_S23); /* 23 */
	MD4GG (b, c, d, a, x[13], MD4_S24); /* 24 */
	MD4GG (a, b, c, d, x[ 2], MD4_S21); /* 25 */
	MD4GG (d, a, b, c, x[ 6], MD4_S22); /* 26 */
	MD4GG (c, d, a, b, x[10], MD4_S23); /* 27 */
	MD4GG (b, c, d, a, x[14], MD4_S24); /* 28 */
	MD4GG (a, b, c, d, x[ 3], MD4_S21); /* 29 */
	MD4GG (d, a, b, c, x[ 7], MD4_S22); /* 30 */
	MD4GG (c, d, a, b, x[11], MD4_S23); /* 31 */
	MD4GG (b, c, d, a, x[15], MD4_S24); /* 32 */

	// Round 3
	MD4HH (a, b, c, d, x[ 0], MD4_S31); /* 33 */
	MD4HH (d, a, b, c, x[ 8], MD4_S32); /* 34 */
	MD4HH (c, d, a, b, x[ 4], MD4_S33); /* 35 */
	MD4HH (b, c, d, a, x[12], MD4_S34); /* 36 */
	MD4HH (a, b, c, d, x[ 2], MD4_S31); /* 37 */
	MD4HH (d, a, b, c, x[10], MD4_S32); /* 38 */
	MD4HH (c, d, a, b, x[ 6], MD4_S33); /* 39 */
	MD4HH (b, c, d, a, x[14], MD4_S34); /* 40 */
	MD4HH (a, b, c, d, x[ 1], MD4_S31); /* 41 */
	MD4HH (d, a, b, c, x[ 9], MD4_S32); /* 42 */
	MD4HH (c, d, a, b, x[ 5], MD4_S33); /* 43 */
	MD4HH (b, c, d, a, x[13], MD4_S34); /* 44 */
	MD4HH (a, b, c, d, x[ 3], MD4_S31); /* 45 */
	MD4HH (d, a, b, c, x[11], MD4_S32); /* 46 */
	MD4HH (c, d, a, b, x[ 7], MD4_S33); /* 47 */
	MD4HH (b, c, d, a, x[15], MD4_S34); /* 48 */

	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;

	// Zeroize sensitive information.
	memset ((MD4_POINTER)x, 0, sizeof(x));
}

// MD4_Encodes input (UINT4) into output (unsigned char). Assumes len is
//     a multiple of 4.
static void MD4_Encode(unsigned char *output, UINT4 *input, unsigned int len)
{
	unsigned int i = 0, j = 0;

	for (i = 0, j = 0; j < len; i++, j += 4)
	{
	output[j] = (unsigned char)(input[i] & 0xff);
	output[j+1] = (unsigned char)((input[i] >> 8) & 0xff);
	output[j+2] = (unsigned char)((input[i] >> 16) & 0xff);
	output[j+3] = (unsigned char)((input[i] >> 24) & 0xff);
	}
}

// MD4_Decodes input (unsigned char) into output (UINT4). Assumes len is
//     a multiple of 4.
static void MD4_Decode(UINT4 *output, unsigned char *input, unsigned int len)
{
	unsigned int i = 0, j = 0;

	for (i = 0, j = 0; j < len; i++, j += 4)
		output[i] = ((UINT4)input[j]) | (((UINT4)input[j+1]) << 8) |
			(((UINT4)input[j+2]) << 16) | (((UINT4)input[j+3]) << 24);
}
//MD4部分结束

//以下内容来自网上资料，在使用openssl库中的DES_ecb_encrypt()函数时出现问题，就引用网上资料中别人实现的功能，原理等同openssl库中的DES_ecb_encrypt()函数

static unsigned char lm_magic[] = { 0x4B, 0x47, 0x53, 0x21, 0x40, 0x23, 0x24, 0x25 };//lm_magic[]实际为"KGS!@#$%"的ASCII码形式，自行定义，用于之后进行加密
//以下部分作用与openssl api中DES.h文件中的函数作用一致
//变换序列
static int ip_data_seq[] = {
    58,50,42,34,26,18,10,2,
    60,52,44,36,28,20,12,4,
    62,54,46,38,30,22,14,6,
    64,56,48,40,32,24,16,8,
    57,49,41,33,25,17,9,1 ,
    59,51,43,35,27,19,11,3,
    61,53,45,37,29,21,13,5,
    63,55,47,39,31,23,15,7};

static int ip_key_seq[] ={
	57,49,41,33,25,17,9,
	1,58,50,42,34,26,18,
	10,2,59,51,43,35,27,
	19,11,3,60,52,44,36,
	63,55,47,39,31,23,15,
	7,62,54,46,38,30,22,
	14,6,61,53,45,37,29,
	21,13,5,28,20,12,4};

static int key_offset[] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
static int ip_key[] ={
    14,17,11,24,1,5,
    3,28,15,6,21,10,
    23,19,12,4,26,8,
    16,7,27,20,13,2,
    41,52,31,37,47,55,
    30,40,51,45,33,48,
    44,49,39,56,34,53,
    46,42,50,36,29,32};

static int ip_e[] = {
    32,1,2,3,4,5,
    4,5,6,7,8,9,
    8,9,10,11,12,13,
    12,13,14,15,16,17,
    16,17,18,19,20,21,
    20,21,22,23,24,25,
    24,25,26,27,28,29,
    28,29,30,31,32,1};
static int ip_p[] = {
    16,7,20,21,29,12,28,17,
    1,15,23,26,5,18,31,10,
    2,8,24,14,32,27,3,9,
    19,13,30,6,22,11,4,25};

static unsigned char s1[64] = {
    14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
    0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
    4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
    15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 };

static unsigned char s2[64] = {
    15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
    3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
    0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
    13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 };

static unsigned char s3[64] = {
    10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
    13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
    13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
    1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 };

static unsigned char s4[64] = {
    7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
    13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
    10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
    3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 };

static unsigned char s5[64] = {
    2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
    14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
    4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
    11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 };

static unsigned char s6[64] = {
    12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
    10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
    9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
    4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13 };

static unsigned char s7[64] = {
    4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
    13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
    1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
    6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 };

static unsigned char s8[64] = {
    13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
    1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
    7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
    2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 };
	
static int inverse_ip_p[64] = {
    40,8,48,16,56,24,64,32,
    39,7,47,15,55,23,63,31,
    38,6,46,14,54,22,62,30,
    37,5,45,13,53,21,61,29,
    36,4,44,12,52,20,60,28,
    35,3,43,11,51,19,59,27,
    34,2,42,10,50,18,58,26,
    33,1,41,9,49,17,57,25};

static void str_to_key (const unsigned char *str, unsigned char *key );
void lm_hash(char * src, unsigned char * dst, int * dst_len);
void algorithm_des(unsigned char * src, unsigned char * secrect,
                   unsigned char * dst);
static void storebit(unsigned char * data, int data_len, unsigned char * dst);
static void parsebit(unsigned char * data, unsigned char * dst, int dst_len);
static void initail_permutation(unsigned char * data, int * schedule, int num,
                                unsigned char * dst);
static void getkey(unsigned char * key,int offset);
static void s_box_function(unsigned char * data, unsigned char * sbox,
                           unsigned char * dst);
unsigned char getbit(unsigned char,int);
void xorbit(unsigned char *,unsigned char *,int,unsigned char *);

//from The Samba Team's source/libsmb/smbdes.c，实现str_to_key()函数

void str_to_key (const unsigned char *str, unsigned char *key )
{
    unsigned int i;
    key[0] = str[0] >> 1;
    key[1] = ( ( str[0] & 0x01 ) << 6 ) | ( str[1] >> 2 );
    key[2] = ( ( str[1] & 0x03 ) << 5 ) | ( str[2] >> 3 );
    key[3] = ( ( str[2] & 0x07 ) << 4 ) | ( str[3] >> 4 );
    key[4] = ( ( str[3] & 0x0F ) << 3 ) | ( str[4] >> 5 );
    key[5] = ( ( str[4] & 0x1F ) << 2 ) | ( str[5] >> 6 );
    key[6] = ( ( str[5] & 0x3F ) << 1 ) | ( str[6] >> 7 );
    key[7] = str[6] & 0x7F;
    for ( i = 0; i < 8; i++ )
    {
        key[i] = ( key[i] << 1 );
    }
    return;
}

//获取16字节的lm-hash内容
void lm_hash(char * src, unsigned char * dst, int * dst_len){
    int i = 0;
    unsigned char lm_src[14];

    //将输入的密码由小写变成大写，数据若不够14位则用0x00补齐
    if(strlen(src) >= 14){
        memcpy(lm_src,src,14);
    }else{
        memset(lm_src,0,14);
        memcpy(lm_src,src,strlen(src));
    }
    for(i = 0 ;i < 14; i ++){
    //  lm_src[i] = toupper(lm_src[i]);
	  if(lm_src[i]>=97&&lm_src[i]<=122)
	  {
		  lm_src[i]=lm_src[i]-32;
	  }
    }

    //将用户密码分成两个7字节的数组调用str_to_key()函数
    str_to_key(lm_src,dst);
    str_to_key(lm_src + 7, dst + 8);

    //分别用上面生成的两个key对magic word进行DES加密，magic word为："KGS!@#$%"
    algorithm_des(lm_magic,dst, dst);
    algorithm_des(lm_magic,dst + 8, dst + 8);
    if(dst_len != NULL)
        * dst_len = 16;
}//LM-HASH end

void algorithm_des(unsigned char * src, unsigned char * secrect,
                   unsigned char * dst){
    unsigned char s[64],key[64],L[32],R[32],K[48],E[48];
    int i = 0;

    //步骤1
    storebit(src,8,s);
	storebit(secrect,8,key);
	initail_permutation(s,ip_data_seq,64,s);
	initail_permutation(key,ip_key_seq,56,key);

	memcpy(L,s,32);
    memcpy(R,s+32,32);
   
    //进行16次计算
    for(i = 0; i < 16 ; i++){
        //获取Ki
        getkey(key,key_offset[i]);
        initail_permutation(key,ip_key,48,K);
        //F计算
        initail_permutation(R,ip_e,48,E);
        xorbit(E,K,48,E);
        s_box_function(E,s1,E);
        s_box_function(E + 6,s2,E + 4);
        s_box_function(E + 12,s3,E + 8);
        s_box_function(E + 18,s4,E + 12);
        s_box_function(E + 24,s5,E + 16);
        s_box_function(E + 30,s6,E + 20);
        s_box_function(E + 36,s7,E + 24);
        s_box_function(E + 42,s8,E + 28);
        initail_permutation(E,ip_p,32,E);

        //更换序列
        xorbit(E,L,32,E);
        memcpy(L,R,32);
        memcpy(R,E,32);
    }

	memcpy(s,R,32);
    memcpy(s+32,L,32);
    initail_permutation(s,inverse_ip_p,64,s);
    parsebit(s,dst,8);
}

//由于后面存在大量的比特操作，而C程序中一般以字节为单位，因此我们将字节中的比特分别
//存贮在8个字节中，以方便后面的大量运算
void storebit(unsigned char * data, int data_len, unsigned char * dst){
    int i = 0;
    for(i = 0 ; i < data_len;i ++){
        dst[i*8] = getbit(data[i],7);
        dst[i*8 + 1] = getbit(data[i],6);
        dst[i*8 + 2] = getbit(data[i],5);
        dst[i*8 + 3] = getbit(data[i],4);
        dst[i*8 + 4] = getbit(data[i],3);
        dst[i*8 + 5] = getbit(data[i],2);
        dst[i*8 + 6] = getbit(data[i],1);
        dst[i*8 + 7] = getbit(data[i],0);
    }
}
//这是storebit得反操作。
void parsebit(unsigned char * data, unsigned char * dst, int dst_len){
    int i = 0;
    for(i = 0 ; i < dst_len ; i ++){
        dst[i] = data[8*i] * 0x80 +
            data[8*i + 1] * 0x40 +
            data[8*i + 2] * 0x20 +
            data[8*i + 3] * 0x10 +
            data[8*i + 4] * 0x8 +
            data[8*i + 5] * 0x4 +
            data[8*i + 6] * 0x2 +
            data[8*i + 7];
    }
}

//移位操作函数
void initail_permutation(unsigned char * data, int * schedule, int num,
                                unsigned char * dst){
    int i = 0;
    unsigned char * temp;
    temp = (unsigned char *)malloc(num);

    for(i = 0 ; i < num; i ++){
        temp[i] = data[schedule[i] - 1];
    }
    memcpy(dst,temp,num);
    free(temp);
}

//左移操作
void getkey(unsigned char * key,int offset){
    unsigned char temp[28];

    memcpy(temp,key + offset,28-offset);
    memcpy(temp + 28 - offset, key , offset);
    memcpy(key,temp,28);

    memcpy(temp,key + 28 + offset,28-offset);
    memcpy(temp + 28 - offset, key + 28 , offset);
    memcpy(key + 28,temp,28);
}

void s_box_function(unsigned char * data, unsigned char * sbox,
                           unsigned char * dst){
    int m = data[0] * 2 + data[5];
    int n = data[1] * 8 + data[2] * 4 + data[3] * 2 + data[4];
    unsigned char c = sbox[m* 16 + n];
    if(c >= 8){
        dst[0] = 1;
        c = c-8;
    }else{
        dst[0] = 0;
    }
    if(c >= 4){
        dst[1] = 1;
        c = c-4;
    }else{
        dst[1] = 0;
    }
    if(c >= 2){
        dst[2] = 1;
        c = c-2;
    }else{
        dst[2] = 0;
    }
    dst[3] = c;
}

unsigned char getbit(unsigned char c,int i)
{
	for(int n=0;n<i;n++)
		c=c>>1;
	return c%2;
}

void xorbit(unsigned char * esrc,unsigned char * ksrc,int i,unsigned char * edst)
{
	for(int n=0;n<i;n++)
		*(edst+n)=*(esrc+n)^*(ksrc+n);
}

